package com.example.c2d.sjetest.Job;

import android.util.Log;

import org.whispersystems.jobqueue.Job;
import org.whispersystems.jobqueue.JobParameters;

public class SampleJob extends Job {

    public SampleJob() {
        super(JobParameters.newBuilder().create());
    }

    @Override
    public void onAdded() {
        // Called after the Job has been added to the queue.
    }

    @Override
    public void onRun() {
        // Here's where we execute our work.
        Log.w("SampleJob", "Hello, world!");
    }

    @Override
    public void onCanceled() {
        // This would be called if the job had failed.
    }

    @Override
    public boolean onShouldRetry(Exception exception) {
        // Called if onRun() had thrown an exception to determine whether
        // onRun() should be called again.
        return false;
    }
}