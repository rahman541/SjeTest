package com.example.c2d.sjetest;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TabHost;
import android.widget.TextView;

import org.whispersystems.jobqueue.JobManager;

import java.lang.reflect.Field;

public class MainActivity extends AppCompatActivity {
    protected DbAdapter db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TabHost host = (TabHost)findViewById(R.id.tabHost);
        host.setup();

        //Tab 1
        TabHost.TabSpec spec = host.newTabSpec("General");
        spec.setContent(R.id.tabGeneral);
        spec.setIndicator("General");
        host.addTab(spec);
        setupGeneralTab();

        //Tab 2
        spec = host.newTabSpec("Tab Two");
        spec.setContent(R.id.tab2);
        spec.setIndicator("Tab Two");
        host.addTab(spec);

        db = new DbAdapter(this);
        //db.insertData("Uname", "Upas");
        Message.message(this, db.getData());
        db.getUser();
    }

    public void setupGeneralTab() {
        TextView lblAndVersion = (TextView)findViewById(R.id.lblAndVersion);
        TextView lblSysArch = (TextView)findViewById(R.id.lblSysArch);

        String osName = Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName();
        lblAndVersion.setText(osName+" (" + Build.VERSION.RELEASE + ") API " + android.os.Build.VERSION.SDK_INT);

        String sysArch = System.getProperty("os.arch");
        lblSysArch.setText(sysArch + Build.CPU_ABI);
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    /* public native String stringFromJNI(); /*

    // Used to load the 'native-lib' library on application startup.
    /* static {
        System.loadLibrary("native-lib");
    } */
}
