package com.example.c2d.sjetest;

import android.app.Application;

import com.example.c2d.sjetest.Job.SampleJob;

import org.whispersystems.jobqueue.JobManager;

/**
 * Created by C2D on 10/12/2017.
 */

public class App extends Application {
    private JobManager jobManager;

    @Override
    public void onCreate() {    // This method will setup for application on install
        super.onCreate();
        initializeJobManager();
    }

    private void initializeJobManager() {
        this.jobManager = JobManager.newBuilder(this)
                .withName("SampleJobManager")
                .withConsumerThreads(5)
                .build();
        // Add Job
        this.jobManager.add(new SampleJob());
    }
}
